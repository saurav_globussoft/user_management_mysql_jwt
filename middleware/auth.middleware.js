const userModel = require("../model/user.model");
const { verifyToken, verifyTokenEmail } = require("../utils/jwt.util");

/**
 * authenticateToken - function to authenticate user
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @returns 
 */
async function authenticateToken(req, res, next) {
    try {
        const authHeader = req.headers['authorization'];
        const tokenFromReq = authHeader.split(" ")[1];

        if (tokenFromReq == null) return res.status(404).json({ code: 404, message: "Access Denied" });

        const payload = verifyToken(tokenFromReq);

        const [checkUserExist] = await userModel.checkUserExistById(payload.id);
        if (!checkUserExist) return res.status(404).json({ code: 404, message: "Invalid user" });
        req.user = payload;
        next();
    } catch (error) {
        next(error);
    }
}


/**
 * authenticateTokenEmail - function to authenticate token from email to reset password
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @returns 
 */
async function authenticateTokenEmail(req, res, next) {
    try {
        const authHeader = req.headers['authorization'];
        const tokenFromReq = authHeader.split(" ")[1];

        if (tokenFromReq == null) return res.status(404).json({ code: 404, message: "Access Denied" });

        const payload = verifyTokenEmail(tokenFromReq);

        const [checkUserExist] = await userModel.checkUserExistById(payload.id);
        if (!checkUserExist) return res.status(404).json({ code: 404, message: "Invalid user" });
        req.user = payload;
        next();
    } catch (error) {
        next(error);
    }
}

module.exports = { authenticateToken, authenticateTokenEmail };