const mysql = require("../sql-test/mySqlConnection");

class UserModel {

    /**
     * getUsers - function to get username and name from database
     * @returns 
     */
    getUsers() {
        const query = `SELECT username, name FROM users`;
        return new Promise((resolve, reject) => {
            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }

    /**
     * getUsersByUname - function to get id, username, name and password from database
     * @param {*} username 
     * @returns 
     */
    getUsersByUname(username) {
        const query = `SELECT id, username, name, password, email FROM users WHERE username = '${username}'`;
        return new Promise((resolve, reject) => {
            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }

    /**
     * checkUserExist - function to get username by matching username
     * @param {*} username 
     * @returns 
     */
    checkUserExist(username) {
        return new Promise((resolve, reject) => {
            if (!username) {
                reject("Error. Please enter username");
            }

            const query = `SELECT username FROM users
                           WHERE username = '${username}';`;
            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }

    /**
     * checkUserExistById - function to get id by matching id
     * @param {*} id 
     * @returns 
     */
    checkUserExistById(id) {
        return new Promise((resolve, reject) => {
            const query = `SELECT id FROM users
                           WHERE id = ${id};`;
            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }

    /**
     * validateUnamePass - function to get username and password by matching username and password
     * @param {*} username 
     * @param {*} password 
     * @returns 
     */
    validateUnamePass(username, password) {
        return new Promise((resolve, reject) => {
            if (!username || !password) {
                reject("Error. Please enter the correct username and password");
            }

            const query = `SELECT username, password FROM users
            WHERE username = '${username}' AND password = '${password}';`;
            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }

    /**
     * checkUnameId - function to get id from username
     * @param {*} username 
     * @returns 
     */
    checkUnameId(username) {
        return new Promise((resolve, reject) => {
            const query = `SELECT id FROM users WHERE username = '${username}';`;
            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }

    /**
     * createUsers - function to create user
     * @param {*} username 
     * @param {*} name 
     * @param {*} password 
     * @returns 
     */
    createUsers(username, name, password, email) {
        return new Promise((resolve, reject) => {
            const query = `INSERT INTO users (username, name, password, email)
                        VALUES ('${username}', '${name}', '${password}', '${email}');`;
            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }

    /**
     * updateUsers - function to update name, email and password
     * @param {*} username 
     * @param {*} name 
     * @param {*} password 
     * @returns 
     */
    updateUsers(username, name, password, email) {
        return new Promise((resolve, reject) => {
            let updateStr = ``;
            if (name) {
                updateStr += updateStr ? ` , name = '${name}' ` : ` SET name = '${name}' `;
            }

            if (password) {
                updateStr += updateStr ? ` , password = '${password}' ` : ` SET password = '${password}' `;
            }

            if (email) {
                updateStr += updateStr ? ` , email = '${email}' ` : ` SET email = '${email}' `;
            }

            const query = `UPDATE users ${updateStr}
                                WHERE username = '${username}';`;

            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }

    /**
     * deleteUsersDb - function to delete user
     * @param {*} id 
     * @returns 
     */
    deleteUsersDb(id) {
        return new Promise((resolve, reject) => {
            const query = `DELETE FROM users WHERE id = '${id}';`;
            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve("User deleted");
            });
        });
    }

    /**
     * resetPassDb - function to reset password
     * @param {*} id 
     * @param {*} password 
     * @returns 
     */
    resetPassDb(id , password) {
        return new Promise((resolve, reject) => {
            const query = `UPDATE users SET password = '${password}' 
                            WHERE id = '${id}';`;
            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve("Password changed");
            });
        });
    }
}

module.exports = new UserModel;
