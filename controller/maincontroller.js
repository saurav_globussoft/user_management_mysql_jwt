const userModel = require("../model/user.model");
const { createToken } = require("../utils/jwt.util");
const encryptDecrypt = require("../services/encryptPass.services");
const { hash } = require("bcrypt");
const { sendEmailLogin, sendEmailVerify } = require("../services/email.services");

class UserMangController {

    /**
     * userLogin - function to login user 
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    async userLogin(req, res) {
        try {
            const { username, password } = req.body;

            const [checkUserExist] = await userModel.checkUserExist(username);
            if (!checkUserExist) return res.status(404).json({ code: 404, message: "Username not found" });

            const [userData] = await userModel.getUsersByUname(username);
            const decrptPass = await encryptDecrypt.decryptPass(password, userData.password);

            const token = createToken(userData.id);
            res.status(200).json({ code: 200, access_token: token });
        } catch (error) {
            res.status(404).json(error);
        }
    }


    /**
     * getUserDetails - function to get user details
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    async getUserDetails(req, res) {
        const userDataDB = await userModel.getUsers();
        return res.status(200).json({ code: 200, data: userDataDB });
    }


    /**
     * createUserData - function to create user
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    async createUserData(req, res) {
        try {
            const { username, password, name, email } = req.body;

            if ((!username) || (!password) || (!name) || (!email)) {
                return res.status(400).json({ code: 400, message: "Please enter Username, Password, Name and Email" })
            }

            const [checkUserExist] = await userModel.checkUserExist(username);
            if (checkUserExist) return res.status(405).json({ code: 405, message: "User Already exist" });

            const pass = await encryptDecrypt.encryptPass(password);
            const createUserData = await userModel.createUsers(username, name, pass, email);

            // const [userData] = await userModel.getUsersByUname(username);
            const aEmail = await sendEmailLogin(username, email, password);

            return res.status(201).json({ code: 201, message: "User added" });
        }
        catch (error) {
            res.status(404).json(error);
        }
    }


    /**
     * updateUserData - function to update user
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    async updateUserData(req, res) {
        try {
            const { id } = req.user;
            const { username, name, password, email } = req.body;

            const [checkUserExist] = await userModel.checkUserExist(username);
            if (!checkUserExist) return res.status(404).json({ code: 404, message: "Username doesn't exist" });

            const [checkValidId] = await userModel.checkUnameId(username);
            if (checkValidId.id != id) return res.status(404).json({ code: 404, message: "Username and ID do not match" });

            const pass = await encryptDecrypt.encryptPass(password);

            const updateUserData = await userModel.updateUsers(username, name, pass, email);
            return res.status(200).json({ code: 200, message: "User updated" });
        }
        catch (error) {
            res.status(404).json(error);
        }
    }


    /**
     * deleteUserData - function to delete user
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    async deleteUserData(req, res) {
        try {
            const { id } = req.user;
            const deleteUserData = await userModel.deleteUsersDb(id);
            return res.status(200).json({ code: 200, message: "User deleted successfully" })
        } catch (error) {
            res.status(404).json(error);
        }
    }


    /**
     * verifyEmail - funtion to very username and email
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    async verifyEmail(req, res) {
        try {
            const { username } = req.body;

            const [checkUserExist] = await userModel.checkUserExist(username);
            if (!checkUserExist) return res.status(404).json({ code: 404, message: "Username not found" });

            const [userData] = await userModel.getUsersByUname(username);

            const aEmail = await sendEmailVerify(userData.email, userData.id);

            return res.status(200).json({ code: 200, message: "Link sent to your email successfully" });
        }
        catch (error) {
            return res.status(404).json(error);
        }
    }


    /**
     * resetPassword - function to reset password
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    async resetPassword(req, res) {
        try {
            const { id } = req.user;
            const { password1, password2 } = req.body;
            if ((!password1) || (!password2)) return res.status(404).json({ code: 404, message: "Enter password and confirm password"})
            if (password1 !== password2) return res.status(405).json({ code: 405, message: "Confirmation password should be same" });

            const pass = await encryptDecrypt.encryptPass(password1);
            const resetPassDb = await userModel.resetPassDb(id, pass);

            return res.status(205).json({ code: 205, message: "Password changed successfully" });

        } catch (error) {
            return res.status(400).json(error);
        }
    }
}

module.exports = new UserMangController;