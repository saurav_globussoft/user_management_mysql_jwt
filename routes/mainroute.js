const router = require("express").Router();

const UserMangController = require("../controller/maincontroller");

const { authenticateToken, authenticateTokenEmail } = require("../middleware/auth.middleware");

router.post("/user", UserMangController.createUserData);

router.post("/user/login", UserMangController.userLogin);

router.get("/user/verify", UserMangController.verifyEmail);

router.put("/user/reset", authenticateTokenEmail, UserMangController.resetPassword);

router.use(authenticateToken);

router.get("/user", UserMangController.getUserDetails);

router.put("/user", UserMangController.updateUserData);

router.delete("/user", UserMangController.deleteUserData);

module.exports = router;