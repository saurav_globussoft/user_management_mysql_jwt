const nodemailer = require("nodemailer");
const { createTokenLimit } = require("../utils/jwt.util")
// const dotenv = require('dotenv').config();


/**
 * autoEmailLogin - function to send mail with credentials
 * @param {*} username 
 * @param {*} email 
 * @param {*} password 
 */
async function sendEmailLogin(username, email, password) {
    try {
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.EMAIL_ID,
                pass: process.env.PASS
            }
        });
        const mailOptions = {
            from: process.env.EMAIL_ID,
            to: email,
            subject: 'New user created',
            text: `Welcome your login credentials are :
                   Username : ${username} and Password : ${password}`
        };
        await transporter.sendMail(mailOptions);
    }
    catch (error) {
        console.log(error);
    }
}


/**
 * autoEmail - function to send email with generated token
 * @param {*} email 
 * @param {*} id 
 */
async function sendEmailVerify(email, id) {
    try {
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.EMAIL_ID,
                pass: process.env.PASS
            }
        });

        const token = createTokenLimit(id);

        const mailOptions = {
            from: process.env.EMAIL_ID,
            to: email,
            subject: 'Email for user verification',
            text: `This token is only valid for 10 mins use it to change password : [ ${token} ]`
        };

        await transporter.sendMail(mailOptions);
    }
    catch (error) {
        console.log(error);
    }
}

module.exports = { sendEmailLogin, sendEmailVerify };