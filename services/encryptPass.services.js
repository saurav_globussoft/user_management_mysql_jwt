const bcrypt = require('bcrypt');

class EncryptDecrypt {
    encryptPass(pass) {
        return new Promise((resolve, reject) => {
            const saltRounds = 10;
            bcrypt.genSalt(saltRounds, function (err, salt) {
                bcrypt.hash(pass, salt, function (err, hash) {
                    resolve(hash);
                });
            });
        });
    }

    decryptPass(password, hash) {
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, hash, function (err, result) {
                if (result) resolve("Password correct");
                reject("Password Incorrect");
            });
        })
    }
}

module.exports = new EncryptDecrypt;