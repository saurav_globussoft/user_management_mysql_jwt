const jwt = require('jsonwebtoken');


/**
 * createToken - function to create token  
 * @param {*} username 
 * @param {*} name 
 * @returns 
 */
function createToken(id) {
    const token = jwt.sign({
        id: id
    }, "secret");
    return token;
}


/**
 * createTokenLimit - function to create token with expiration time of 10 mins
 * @param {*} id 
 * @returns 
 */
function createTokenLimit(id) {
    return jwt.sign({
        id: id
    }, "secret1", { expiresIn: 60 * 10 }); 
}


/**
 * verifyToken - function to verify token
 * @param {*} token 
 * @returns 
 */
function verifyToken(token) {
    return jwt.verify(token, 'secret');
}


/**
 * verifyTokenEmail - function to verify token from email to reset password
 * @param {*} token 
 * @returns 
 */
function verifyTokenEmail(token) {
    return jwt.verify(token, 'secret1');
}

module.exports = { createToken, verifyToken, createTokenLimit, verifyTokenEmail };